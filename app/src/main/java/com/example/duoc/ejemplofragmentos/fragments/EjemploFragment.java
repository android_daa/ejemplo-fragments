package com.example.duoc.ejemplofragmentos.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.duoc.ejemplofragmentos.R;

/**
 * Created by DUOC on 13-05-2017.
 */

public class EjemploFragment extends Fragment {

    private Button btnSaludo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ejemplo, container, false);

        btnSaludo = (Button)view.findViewById(R.id.btnSaludo);
        btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragment();

            }
        });
        return view;
    }

    private void cambiarFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, new DetailsFragment(), "fragment_detalle");
        ft.addToBackStack(null);
        ft.commit();
    }
}
