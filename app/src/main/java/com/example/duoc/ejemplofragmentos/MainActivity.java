package com.example.duoc.ejemplofragmentos;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.duoc.ejemplofragmentos.fragments.DetailsFragment;
import com.example.duoc.ejemplofragmentos.fragments.EjemploFragment;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabs;
    private ViewPager viewPager;
    private MyPageAdapter myPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Ejemplo Fragmentos");
        setSupportActionBar(toolbar);
        initTabs();
        initPager();
    }

    private void initTabs(){
        tabs = (TabLayout) findViewById(R.id.tabs);

        tabs.addTab(tabs.newTab().setText("TELÉFONOS").setIcon(R.mipmap.ic_launcher));
        tabs.addTab(tabs.newTab().setText("TABLETS").setIcon(R.mipmap.ic_launcher));
        tabs.addTab(tabs.newTab().setText("PORTÁTILES").setIcon(R.mipmap.ic_launcher));
    }

    private void initPager(){
        viewPager = (ViewPager)findViewById(R.id.pager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        myPageAdapter = new MyPageAdapter(getSupportFragmentManager());
        myPageAdapter.addFragment(new EjemploFragment(), "uno");
        myPageAdapter.addFragment(new DetailsFragment(), "dos");
        myPageAdapter.addFragment(new EjemploFragment(), "tres");
        viewPager.setAdapter(myPageAdapter);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }

    }
}
